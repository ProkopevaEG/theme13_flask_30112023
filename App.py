from flask import Flask, render_template, request

from processing import get_message

App = Flask(__name__)

#http://127.0.0.1:5000 + '/' = http://127.0.0.1:5000/

@App.route('/', methods=['get', 'post'])
def main():
    message = ''
    if request.method == "POST":
        n = int(request.form.get("n"))
        message = get_message(n)

    return render_template('index.html', message=message)


@App.route('/text')  #http://127.0.0.1:5000 + '/text' = http://127.0.0.1:5000/text
def text():
    return "Text"

App.run()
